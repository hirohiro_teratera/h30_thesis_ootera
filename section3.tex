% !TEX root = main.tex

\lstset{
	numbers = {left},
	frame = {tb},
	xleftmargin = 3zw
}
\section{モデル可視化ツールの開発}
\subsection{枠組み}
本節では，開発したモデル可視化ツールにおける NuSMV の入力モデルから状態遷移グラフを抽出する枠組みについて述べる．
図\ref{fig:modelvisiblesoftware}にモデル可視化ツールの枠組みを示す．

\begin{figure}[h]
	\centering
	\includegraphics[width=1\linewidth]{pictures/model_visible_software}
	\caption{モデル可視化ツール}
	\label{fig:modelvisiblesoftware}
\end{figure}

まず，可視化の対象となる NuSMV の入力モデル (SMV プログラム) において，状態遷移に着目したい変数名を選択する．
その上で，SMV プログラムと着目変数の変数名を開発したモデル可視化ツールへと入力する．
ツールは SMV プログラムの構文解析を行い，着目変数の状態遷移についての情報を SMV プログラムから抽出する．
次に，抽出した情報をもとに，SMT ソルバへの入力となる遷移関係を表す論理式を生成する．
SMT ソルバにより着目変数の遷移を抽出した後，状態遷移グラフを描画する．

以下，ソースコード\ref{sample}に示す例を用いて，SMV プログラムから状態遷移グラフを生成する手順について示す．

\begin{lstlisting}[caption=サンプルプログラム,label=sample]
MODULE main
VAR
	a : 0 .. 2;

ASSIGN
	init(a) :=0;
	next(a) := case
		a < 2 : a + 1;
		a = 2 : 0;
		TRUE  : a;
	esac;
\end{lstlisting}


\subsection{着目変数に関する情報の抽出}
ここで，着目変数をソースコード\ref{hyou}内の変数 \texttt{a} とする．
変数の状態遷移をグラフとして描画する際に必要となる情報は，変数名とその定義域，そして遷移ルールとして記述された条件式と右辺値となる．
変数名とその定義域は \texttt{VAR} による変数宣言部から抽出する．
条件式と右辺値は \texttt{ASSIGN} による遷移記述部から抽出する．

着目変数を \texttt{a} とした場合，SMV プログラムから抽出される情報は表\ref{hyou}に示す通りとなる．

\begin{table}[H]
	\centering
	\caption{着目変数について抽出される情報}
	\label{hyou}
		\begin{tabular}{|c|c|c|c|}
			\hline
			変数                 & 定義域                  & 条件式          & 右辺式   \\ \hline
			\multirow{3}{*}{a} & \multirow{3}{*}{[0,2]} & a \textless 2 & a + 1 \\ \cline{3-4} 
			&                      & a = 2         & a = 0 \\ \cline{3-4} 
			&                      & TRUE          & a     \\ \hline
		\end{tabular}
\end{table}

\subsection{SMTソルバを用いた遷移の抽出}
着目変数に関する遷移関係を論理式として表現し，SMT ソルバを用いて遷移を抽出するための手法について説明する．
変数 \texttt{a} の現在の値を $ \mathrm{a} $，遷移後の値を $ \mathrm{a}^{\prime} $ でそれぞれ表す．
$ \mathrm{a}^{\prime} $は， \texttt{a} の条件式が満たされたとき，対応する右辺式の値となる．
NuSMV では先に記述された遷移ルールが優先されるため，変数 \texttt{a} の遷移関係は以下の論理式として表現できる．

\begin{eqnarray*}
&&(\mathrm{a} < 2 \Rightarrow \mathrm{a}^{\prime} = \mathrm{a} + 1)\\
&\wedge &(\neg(\mathrm{a} < 2) \wedge \mathrm{a} = 2 \Rightarrow \mathrm{a}^{\prime} =0)\\
&\wedge &(\neg(\mathrm{a} < 2) \wedge \neg(\mathrm{a} = 2) \Rightarrow \mathrm{a}^{\prime} = \mathrm{a} ) 
\end{eqnarray*}

ここで，$ \mathrm{a} = 0 $ である状態から $ \mathrm{a} = 1 $ である状態への遷移が存在するか否かを判定することを考える．
これは，上述の遷移関係とこの遷移に対応する値の変化がともに充足可能か否かを判定すれば良い．
したがって，以下の論理式の充足可能性を SMT ソルバにより判定する．
\begin{eqnarray*}
&& (\mathrm{a} < 2 \Rightarrow \mathrm{a}^{\prime} = \mathrm{a} + 1)\\
&\wedge &(\neg(\mathrm{a} < 2)\wedge \mathrm{a} = 2 \Rightarrow \mathrm{a}^{\prime} =0)\\
&\wedge &(\neg(\mathrm{a} < 2) \wedge \neg(\mathrm{a} = 2) \Rightarrow \mathrm{a}^{\prime} = \mathrm{a})\\
&\wedge &( \mathrm{\mathrm{a}} = 0 \wedge  \mathrm{a}^{\prime} = 1 )
\end{eqnarray*}

この論理式は充足可能であるため，$ \mathrm{a} $が$ 0 $から$ 1 $となる遷移が存在することが確認できる．
同様に，$ \mathrm{a} $の定義域の情報をもとに，現状態と次状態のすべての組み合わせについて充足可能性を判定することで，$ \mathrm{a} $の全遷移の存在を確認することができる．
$ \mathrm{a} $は$ 0 \sim 2 $の値をもつので，$ (\mathrm{a},\mathrm{a}^{\prime}) = (0,1),(0,2),(0,3), \ldots, (3,3) $ のそれぞれの組み合わせに対して論理式を作成し，SMT ソルバによる充足可能性判定を行うことで，変数 \texttt{a} の全ての遷移を抽出することができる．
この例では，$ (\mathrm{a},\mathrm{a}^{\prime}) = (0,1),(1,2),(2,0)$ について論理式が充足可能となり，3 つの遷移が存在することが示された．


\subsection{状態遷移グラフの作成}
状態遷移グラフは変数の状態を表す丸型のノードと状態間の遷移を表すエッジから構成される．
ノードには変数の状態を表すラベルが付加される．
条件式に着目変数以外の変数が含まれる場合は，エッジにはその条件を表すラベルが付加される．
上述の通り，この例では $ (\mathrm{a},\mathrm{a}^{\prime}) = (0,1),(1,2),(2,0)$ の 3 つの遷移が存在することが SMT ソルバにより示されているため，求める状態遷移グラフを図\ref{fig:samplea}のように得ることができる．

\begin{figure}
	\centering
	\includegraphics[scale=0.5]{pictures/sample_a}
	\caption{変数\texttt{a}の状態遷移グラフ}
	\label{fig:samplea}
\end{figure}


