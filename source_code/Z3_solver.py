from z3 import *
def solver_make():
    s=Solver()
    n_0,(Small_0,Just_0,Large_0) = EnumSort("n_0",["Small_0","Just_0","Large_0"])
    n,n_next = Consts("n n_next",n_0)
    pc_1,(L1_1,L2_1,L3_1,L4_1,L5_1) = EnumSort("pc_1",["L1_1","L2_1","L3_1","L4_1","L5_1"])
    pc,pc_next = Consts("pc pc_next",pc_1)
    list = [n,pc]
    next_list = [n_next,pc_next]
    s.add(Implies(And(pc==L1_1,Or(n==Small_0,n==Large_0)),pc_next == L2_1))
    s.add(Implies(And(Not(And(pc==L1_1,Or(n==Small_0,n==Large_0))),And(pc==L1_1,Not(Or(n==Small_0,n==Large_0)))),pc_next == L5_1))
    s.add(Implies(And(Not(And(pc==L1_1,Or(n==Small_0,n==Large_0))),Not(And(pc==L1_1,Not(Or(n==Small_0,n==Large_0)))),And(pc==L2_1,n==Small_0)),pc_next == L3_1))
    s.add(Implies(And(Not(And(pc==L1_1,Or(n==Small_0,n==Large_0))),Not(And(pc==L1_1,Not(Or(n==Small_0,n==Large_0)))),Not(And(pc==L2_1,n==Small_0)),And(pc==L2_1,n!=Small_0)),pc_next == L4_1))
    s.add(Implies(And(Not(And(pc==L1_1,Or(n==Small_0,n==Large_0))),Not(And(pc==L1_1,Not(Or(n==Small_0,n==Large_0)))),Not(And(pc==L2_1,n==Small_0)),Not(And(pc==L2_1,n!=Small_0)),pc==L3_1),pc_next == L1_1))
    s.add(Implies(And(Not(And(pc==L1_1,Or(n==Small_0,n==Large_0))),Not(And(pc==L1_1,Not(Or(n==Small_0,n==Large_0)))),Not(And(pc==L2_1,n==Small_0)),Not(And(pc==L2_1,n!=Small_0)),Not(pc==L3_1),pc==L4_1),pc_next == L1_1))
    s.add(Implies(And(Not(And(pc==L1_1,Or(n==Small_0,n==Large_0))),Not(And(pc==L1_1,Not(Or(n==Small_0,n==Large_0)))),Not(And(pc==L2_1,n==Small_0)),Not(And(pc==L2_1,n!=Small_0)),Not(pc==L3_1),Not(pc==L4_1),pc==L5_1),pc_next == L5_1))
    s.add(Implies(pc==L3_1,Or(n_next == Small_0,n_next == Just_0)))
    s.add(Implies(And(Not(pc==L3_1),pc==L4_1),Or(n_next == Large_0,n_next == Just_0)))
    s.add(Implies(And(Not(pc==L3_1),Not(pc==L4_1)),n_next == n))
    return (s,list,next_list)