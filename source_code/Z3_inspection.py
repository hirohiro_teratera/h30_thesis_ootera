from Z3_solver import *
def Z3_inspect():
    check_list = []
    s,list,next_list = solver_make()
    n_0,(Small_0,Just_0,Large_0) = EnumSort("n_0",["Small_0","Just_0","Large_0"])
    n,n_next = Consts("n n_next",n_0)
    pc_1,(L1_1,L2_1,L3_1,L4_1,L5_1) = EnumSort("pc_1",["L1_1","L2_1","L3_1","L4_1","L5_1"])
    pc,pc_next = Consts("pc pc_next",pc_1)
    list = [n,pc]
    next_list = [n_next,pc_next]
    for n_val in (Small_0, Just_0, Large_0):
        for pc_val in (L1_1, L2_1, L3_1, L4_1, L5_1):
            for n_val_next in (Small_0, Just_0, Large_0):
                for pc_val_next in (L1_1, L2_1, L3_1, L4_1, L5_1):
                    s.push()
                    s.add(And(list[0] == n_val,next_list[0] == n_val_next,list[1] == pc_val,next_list[1] == pc_val_next))
                    s_check = s.check()
                    if s_check == sat:
                         if str(n_val) != str(n_val_next):
                              check_list.append(str(n_val)+"," +str(n_val_next)+","+str(pc_val))
                    s.pop()
    return check_list