import re
import pydotplus as pydot
from PIL import Image
from z3 import *
import os
#ファイル読み込み
#文字列からリスト作成(変数宣言部の文字列処理)返り値:リスト
def filetolist():
    mylist =[]
    print("smvファイルを入力してください")
    ftmp = input()#入力は絶対参照ファイルで
    with open(ftmp,'r') as f:
        for row in f:
            list_tmp = [x.strip() for x in
                re.split(':|;|,|{|}|\n',row) if not x.strip() == '']
            mylist.append(list_tmp)
    return mylist,ftmp

#mylist内の空白リスト削除
def list_null_del(list):
    list = [x for x in list if x]
    return list

#初期値格納リストを作成
#返り値は初期値のみを格納したリスト[[変数、値],[],..]
def init_extraction(mylist,val_list):
    init_list=[]
    val_check = False #変数が見つかったときチェック
    tmp_list =[]      #一つの初期値を格納するためのリスト
    for row in mylist:
        if tmp_list:
            init_list.append(tmp_list)
            tmp_list = []
        val_check = False
        tmp = []
        for row_index in row:
            #初期変数を格納後、row_indexは"=値"なので、tmp_listに格納
            if val_check :
                tmp = row_index.split("=")
                #print("---tmp内---")
                #print(tmp)
                if len(tmp) > 1:
                    if tmp[1] != "": #空白を追加しないため
                        tmp_list.append(tmp[1])
                else:
                    tmp_list.append(row_index)

            if row_index.find("init") !=-1: #文字列内にinitが存在する
                #初期値と変数を格納
                #val_list内に変数があるのか確認
                if len(val_list) > 1:
                    for row_val in val_list:
                        if len(row_val) > 1:
                            #val_list内に格納した変数が
                            #見つかったら、init_listに格納
                            if row_index.find(row_val[0]) != -1:
                                tmp_list.append(row_val[0])
                                val_check = True
    return init_list

#変数格納関数val_list=[[変数","値",..],[],..]
def val_storage(mylist):
    check = False
    val_list = []
    tmp_list = []
    for row in mylist:
        #ここで、val_listを二重リストでtmp_listから変数を受け渡す
        if check == True  :
            #tmp_listに変数が格納しているか判断
            if tmp_list:
                val_list.append(tmp_list)
                tmp_list =[]
        #tmp_listに一つの変数を格納
        for row_index in row:
            #ASSIGNの行になるまで変数宣言部である
            if row_index.find("ASSIGN") != -1:
                check = False
            #変数宣言部の行なので、val_listに格納
            if check == True:
                tmp_list.append(row_index)
            #変数宣言部の行であるため、チェック
            if row_index.find("VAR") != -1:
                 check = True

    return val_list

#変数格納関数val_list=[[変数","タイプ","値",..],[],..]
#タイプ
#boolean:ブール型(boolean)
#{<シンボル>,<シンボル>}:列挙型(enum)
#<整数1>..<整数2>:<整数1>以上<整数2>(range)
#{<整数>,<整数>}：整数列挙型(enum_int)
#変数格納リストval_listにタイプを追加する
def val_list_extantion(val_list):
    for row_number,row in enumerate(val_list):
        tmp = []
        tmp_space = []
        index_start = False #行の0番目を無視するための変数
        range_check = False
        enum_check =False
        for row_index in row:

            #0番目ではない
            if index_start:

                #boolean型であるか
                if row_index ==  "boolean":
                    pass

                #<整数1>以上<整数2>(range)であるか
                elif row_index.find("..") != -1:
                    range_check = True
                    #".."で分割
                    tmp = row_index.split("..")
                    #スペースで分割
                    if len(tmp) > 1:
                        tmp_space = tmp[0].split()
                        tmp[0] = tmp_space[0]
                        tmp_space = tmp[1].split()
                        tmp[1] = tmp_space[0]

                #列挙型であるか
                else:
                    enum_check = True

            index_start = True
        if range_check:
            #val_listに"range",tmpを追加する
            #ex. [["変数","range","整数1","整数2"]]
            if len(val_list) > row_number :
                if len(val_list[row_number]) > 1:
                    val_list[row_number].pop(1)
                    val_list[row_number].append("range")
                    val_list[row_number].append(tmp[0])
                    val_list[row_number].append(tmp[1])
        #列挙型である時
        if enum_check:
            enum_enum_check = False
            #列挙型、整数列挙型であるか確認
            for index_number,index in enumerate(row):
                if index_number > 0:
                    if index.isdigit() == False:
                        enum_enum_check = True
            #列挙型
            if enum_enum_check:
                if len(val_list[row_number]) > 1 :
                    val_list[row_number].insert(1,"enum")
            #整数列挙型
            else:
                if len(val_list[row_number]) > 1 :
                    val_list[row_number].insert(1,"enum_int")

    return val_list

#case_extractionの拡張版
#着目したい変数関係なしにすべての遷移をnext_listを追加できる
def case_extraction_ext(mylist,val_list):
    case_list = []
    tmp_list = []
    tmp_name = []
    case_check = False
    no_case_check = False
    for row in mylist:

        if tmp_name:
            if case_check:
                case_list.append(tmp_name)

        if tmp_list:
            case_list.append(tmp_list)
            tmp_list = []

        next_check = False
        tmp_name = []

        for row_index in row:
            #nextであるか判別
            if row_index.find("next") != -1:
                #どの変数の遷移の記述か判別するため、
                #その変数を判別し、tmp_listに追加
                split_tmp = row_index.split("(")
                if len(split_tmp) > 1:
                    for val_row in val_list:
                        if split_tmp[1].find(val_row[0]) != -1:
                            tmp_name.append(val_row[0])
                next_check = True

            if row_index.find("esac") != -1:
                case_check = False

            if case_check:
                tmp_list.append(row_index)

            if next_check:
                if row_index.find("case") != -1:
                    case_check = True

        if case_check:
            if len(tmp_list) > 1:
                tmp_list.insert(1,":")

    return case_list

#nextにおける定数をまとめるリスト
#戻り値：constant_list=[[変数][":",,"値",...],...]
def constant_extraction(mylist,val_list):
    constant_list = []
    tmp_nocase_list = []
    tmp_name = []
    tmp_list = []
    constant_check =False
    next_check = False
    for row in mylist:
        #print("---tmp_name---")
        #print(tmp_name)
        #nextを発見
        if next_check:
            #ここで、定数なのかcase文なのか判別する
            for index in tmp_nocase_list:
                #print("---index---")
                #print(index)
                if index.find("case") == -1:
                    if index.find("=") == -1:
                        tmp_list.append(index)
                        constant_check = True

        if constant_check:
            tmp_list.insert(0,":")
            #print("---tmp_list---")
            #print(tmp_list)
            constant_list.append(tmp_name)
            constant_list.append(tmp_list)
            tmp_list = []
            constant_check = False

        #row_indexのループが終わる度に初期化
        next_check = False
        tmp_name = []
        tmp_nocase_list = []

        for row_index in row:

            if next_check:
                tmp_nocase_list.append(row_index)
                #print("---tmp_nocase_list---")
                #print(tmp_nocase_list)

            if row_index.find("next") != -1:
                #print("---row_index---")
                #print(row_index)
                for val_row in val_list:
                    if row_index.find(val_row[0]) != -1:
                        tmp_name.append(val_row[0])
                next_check =True

    return constant_list

#next_listを作成
#next_list:case_listとconstant_listを統合したリスト
def next_list_make(case_list,constant_list):
    next_list = []
    for row in case_list:
        next_list.append(row)
    for row in constant_list:
        next_list.append(row)

    return next_list

#ソルバの変数を宣言するファイルを
#生成するためのリストを作成する関数
def solver_val_def_file_write(val_list,case_list,solver_py_file_list):
    #変数を作るための書き出し
    list_str = "    list = ["
    list_next_str = "    next_list = ["
    seisu_check = False
    for row_number,row in enumerate(val_list):
        seisu_check = False
        enum_int_check = False
        tmp_str = ""
        tmp_next_str = ""
        add_tmp = "    s.add("
        add_next_tmp = "    s.add("
        if row_number > 0:
            list_str = list_str + ","
            list_next_str = list_next_str + ","
        list_str = list_str + row[0]
        list_next_str = list_next_str + row[0] +"_next"
        #範囲型変数を宣言
        if row[1] == "range":
            tmp_str = "    " + row[0] + "= Int(\""  + row[0] + "\")"
            tmp_next_str = "    " + row[0] \
                    + "_next = Int(\""  + row[0] + "_next\")"
            add_tmp = add_tmp + row[0] + ">=" + row[2] +"," \
                    + row[0] + "<=" + row[3] + ")"
            add_next_tmp = add_next_tmp +  row[0] + "_next>=" \
                    + row[2] +"," + row[0] \
                    + "_next<=" + row[3] + ")"
            seisu_check = True
        #boolean型変数を宣言
        elif row[1] == "boolean":
            tmp_str = "    " + row[0] + "=Bool(\"" + row[0] + "\")"
            tmp_next_str = "    " + row[0] + "_next = Bool(\"" \
                    + row[0] + "_next\")"
        #列挙型変数を宣言
        elif row[1] == "enum":
            tmp_enum_list = []
            tmp_str =  "    " + row[0]+ "_" + str(row_number) + ",("
            for index_number,index in enumerate(row):
                if index_number > 1:
                    tmp_enum_list.append(index)

            for index_number,index in enumerate(tmp_enum_list):
                if index_number > 0:
                    tmp_str = tmp_str + ","
                #数字のみの変数を作ることを避けるため
                if index.isdigit() == True:
                    tmp_str = tmp_str +"buf_" + index
                else:
                    tmp_str = tmp_str + index
            tmp_str = tmp_str + ") = EnumSort(\"" + row[0]+ "_" \
                    + str(row_number) + "\",["

            for index_number,index in enumerate(tmp_enum_list):
                if index_number > 0:
                    tmp_str = tmp_str + ","
                tmp_str = tmp_str + "\"" + index + "\""

            tmp_str = tmp_str + "])"
            tmp_next_str = "    " + row[0] + "," + row[0] \
                    + "_next = Consts(\"" + row[0] \
                    + " " + row[0] + "_next\"," +row[0]\
                    + "_" + str(row_number)+ ")"
        #整数列挙型を宣言
        elif row[1] == "enum_int":
            tmp_str = "    " + row[0] + "= Int(\""  + row[0] + "\")"
            tmp_next_str = "    " + row[0] + "_next = Int(\""  \
                    + row[0] + "_next\")"
            add_tmp = add_tmp + "Or("
            add_next_tmp = add_next_tmp +  "Or("
            for index_number,index in enumerate(row):
                if index_number > 2:
                    add_tmp = add_tmp + ","
                    add_next_tmp = add_next_tmp + ","
                if index_number > 1:
                    add_tmp = add_tmp + row[0] + "==" + index
                    add_next_tmp = add_next_tmp + row[0]\
                        + "_next==" + index
            add_tmp = add_tmp + "))"
            add_next_tmp = add_next_tmp +  "))"
            enum_int_check = True
        solver_py_file_list.append(tmp_str)
        solver_py_file_list.append(tmp_next_str)
        if seisu_check | enum_int_check:
            solver_py_file_list.append(add_tmp)
            solver_py_file_list.append(add_next_tmp)
    list_str = list_str + "]"
    list_next_str = list_next_str + "]"
    solver_py_file_list.append(list_str)
    solver_py_file_list.append(list_next_str)
    #print("---solver_py_file_list---")
    #print(solver_py_file_list)

    return solver_py_file_list

#遷移条件における制約をソルバに追加することをファイルに書き出す関数
def solver_change_def_file_write(val_list,val_z3_list,
    case_list,solver_py_file_list):
    #遷移条件を書き出す
    Not_change_list = []
    for row in case_list:
        add_tmp = "    s.add(Implies("
        semikoron_check  = False
        #遷移条件を記載している行なのか判断:
        if len(row) > 1:
            kanma_number = 0
            for number,index in enumerate(row):
                #遷移条件を追加していく
                if number == 0:
                    if search_val_kaisuu != 0:
                        add_tmp = add_tmp + "And("
                    else:
                        Not_change_list = []

                    smv_code_list = smv_code_list_make(index)
                    smv_code_list = list_null_del(smv_code_list)
                    #print("---smv_code_list---")
                    #print(smv_code_list)

                    Log_exp_list=Log_exp_list_make(val_list,
                                            val_z3_list,smv_code_list)
                    #print("---Log_exp_list---")
                    #print(Log_exp_list)

                    nomove_Log_exp_list=not_operand_move(Log_exp_list)
                    #print("---nomove_Log_exp_list---")
                    #print(nomove_Log_exp_list)

                    rpn_expression_list = getrpn(Log_exp_list)
                    #print("---rpn_expression_list---")
                    #print(rpn_expression_list)

                    Solver_Log_exp = rpn_to_soleve(rpn_expression_list)
                    #print("---Solver_Log_exp---")
                    #print(Solver_Log_exp)

                    if len(Solver_Log_exp) > 0:
                        not_tmp = ""
                        if search_val_kaisuu != 0:
                         for number,index in enumerate(Not_change_list):
                            if number >0:
                                not_tmp = not_tmp +","
                            not_tmp = not_tmp + index

                        tmp = Solver_Log_exp[0]
                        Not_change_list.append("Not(" + tmp + ")")
                        #print("---Not_change_list---")
                        #print(search_val_kaisuu,Not_change_list)
                        if len(not_tmp) > 0:
                            if tmp == "True" :
                                add_tmp = add_tmp +not_tmp + ")"
                            else:
                                add_tmp=add_tmp+not_tmp+","+tmp+")"
                        else:
                            add_tmp = add_tmp + tmp


                #右辺式を書き出すブロック
                if semikoron_check:
                    if kanma_number > 0:
                        add_tmp = add_tmp + ","
                    #["pc==L1",":","Large","Small"]のとき、
                    #右辺式に複数あるため、それをORで囲む
                    elif kanma_number == 0:
                        if len(row) > 3:
                            add_tmp = add_tmp + "Or("
                    #print("---右辺式---")
                    #print(index)
                    #index(右辺式)をソルバで表現できるように変換
                    z3_exp_str = Log_exp_to_z3_exp(index)
                    #print(z3_exp_str)
                    add_tmp = add_tmp + val_list[search_number][0]\
                            + "_next == " + z3_exp_str
                    kanma_number = kanma_number + 1
                #分岐確認ブロック
                if index == ":":
                    #print("check")
                    semikoron_check  = True
                    add_tmp = add_tmp + ","

            #Orの()閉じるをを行うための分岐
            if len(row) > 3:
                add_tmp = add_tmp + ")"
            #最後,()を閉じるを行い、リストに追加
            add_tmp = add_tmp + "))"
            solver_py_file_list.append(add_tmp)
            search_val_kaisuu = search_val_kaisuu + 1
        #右辺式で変化する変数を格納するためのブロック
        else:
            number = 0
            for number,search_row in enumerate(val_list):
                if row[0] == search_row[0]:
                    search_number = number
                    search_val_kaisuu = 0

    solver_py_file_list.append("    return (s,list,next_list)")

    return solver_py_file_list

#与えられた論理式(文字列)をz3のソルバで適用できるように変換する
#引き値：論理式の文字列 ex. (a + 1) mod 3　
#返り値：z3で表現可能な論理式の文字列 ex. (a + 1) % 3　
def Log_exp_to_z3_exp(Log_exp_str):
    z3_exp_str = Log_exp_str

    #mod用
    tmp = Log_exp_str.split("mod")
    if len(tmp) > 1:
         for number,index in enumerate(tmp):
             if number !=  0:
                 z3_exp_str = z3_exp_str + "%" + index
             else:
                 z3_exp_str =  index

    #TRUEをTrueに変換
    if Log_exp_str == ("TRUE"):
        z3_exp_str = Log_exp_str.replace("TRUE","True")
    elif Log_exp_str.find("TRUE") != -1:
        z3_exp_str = Log_exp_str.replace("TRUE","True")
    #FALSEをFalseに変換
    if Log_exp_str == ("FALSE"):
        z3_exp_str = Log_exp_str.replace("FALSE","False")
    elif Log_exp_str.find("FALSE") != -1:
        z3_exp_str = Log_exp_str.replace("FALSE","False")

    return z3_exp_str
#着目した変数を入力するための関数
def search_val_list_func(val_list):
    #入力されたソースコード内の変数をコマンドプロンプトに出力
    val_print(val_list)
    print("着目したい変数を入力してください:ex.変数,変数,...")
    check = False
    tmp = input()
    search_val_list = tmp.split(",")
    for index in search_val_list:
        for val_row in val_list:
            if len(val_row) > 1:
                if index == val_row[0]:
                    check = True
        if check == False:
            print("その変数は存在しません")
            sys.exit()
    return search_val_list

def label_list_make(next_list,val_list ,search_val_list):
    val_other_list = []
    tmp_list = []
    label_list = []
    check = False

    for row in val_list:
        val_other_list.append(row[0])

    for search_index in search_val_list:
        val_other_list.remove(search_index)

    #print("---val_other_list---")
    #print(val_other_list)

    for row in next_list:
        for index in search_val_list:
            if row[0] == index:
                check = True
                continue
        for search_index in val_other_list:
            if row[0] == search_index:
                check = False
                continue

        if check:
            #print("label_list 内")
            #print(row[0])
            for search_index in val_other_list:
                if row[0].find(search_index) != -1:
                    label_list.append(search_index)

    if label_list:
        label_list =set(label_list)
    tmp_list = label_list
    label_list = []
    for val_row in val_list:
        for index in tmp_list:
            if val_row[0] == index:
                label_list.append(val_row[0])
    return label_list

#論理式を一度逆ポーランド記法に変換
#引き値：論理気の文字列リスト:Logical_expression
#ex ["(","digit<2", "&", "digit1 ==2",")"]
def getrpn(Log_exp_list):
    stack = []
    buffer = []

    for token in Log_exp_list:
        if token == '(':
            stack.append(token)
        elif token == ')':
            while len(stack) > 0:
                tmp = stack.pop()
                if tmp == '(':
                    break
                else:
                    buffer.append(tmp)
        elif  token == "!":
            buffer.append(token)
        elif token == '&' or token == '|':
            while len(stack) > 0:
                if stack[-1] == '&' or stack[-1] == '|':
                    buffer.append(stack.pop())
                else:
                    break
            stack.append(token)
        else:
            buffer.append(token)

    while len(stack) > 0:
        buffer.append(stack.pop())
    rpn_expression_list = buffer
    return rpn_expression_list


#逆ポーランド記法を計算する関数
def rpn_to_soleve(rpn_expression_list):
    stack = []
    Solver_Log_exp = []
    tmp = ""
    if len(rpn_expression_list) == 1:
        Solver_Log_exp = rpn_expression_list
    else:
        for  number ,index in enumerate(rpn_expression_list):
            if index  not in ("&" , "|" , "!"):
                stack.append(index)
                continue

            #print("what operand?")
            #print(index)
            if index ==  "!":
                p = stack.pop()
                #print(index)
                tmp = "Not(" + p + ")"
                #print("---!---")
                #print(tmp)
            elif index == "&":
                q = stack.pop()
                p = stack.pop()
                tmp = "And(" + p + "," + q + ")"
            elif index == "|":
                q = stack.pop()
                p = stack.pop()
                tmp = "Or(" + p +"," + q + ")"
            stack.append(tmp)
            #print("---tmp---")
            #print(tmp)

            Solver_Log_exp = stack

    return Solver_Log_exp

def Log_exp_list_make(val_list,val_z3_list,smv_code_list):
    operand_check = False
    tmp = []
    #"!","=fa"のときに、"!=fa"と変換する
    for r_number,row in enumerate(smv_code_list):
        if row.find("=") == 0:
            if smv_code_list[r_number-1] == "!":
              smv_code_list[r_number]=smv_code_list[r_number-2]\
                        + "!" + smv_code_list[r_number]
              smv_code_list.pop(r_number-1)
              smv_code_list.pop(r_number-2)
    for r_number,row in enumerate(smv_code_list):
        #=を==に表記するため
        operand_check = False
        for number,index in enumerate(row):
            if index in (">","<","!"):
                operand_check = True
            if operand_check == False :
                if index == "=":
                    tmp = smv_code_list[r_number]
                    smv_code_list[r_number]=tmp[:number] \
                            + "=" + tmp[number:]
            smv_code_list[r_number]=Log_exp_to_z3_exp(
                                    smv_code_list[r_number])
            smv_code_list[r_number] = Log_val_name_change(val_list,
                                val_z3_list,smv_code_list[r_number])
    Log_exp_list = smv_code_list
    return Log_exp_list

def smv_code_list_make(smv_code):
    smv_code_list = []
    end_logic_number = 0
    for number,index in enumerate(smv_code):
        if index in ("&","|","!","(",")"):
            tmp = ""
            for insert_number,insert_index in enumerate(smv_code):
                if insert_number >= end_logic_number:
                    if insert_number < number:
                        if insert_index != " ":
                            tmp = tmp + insert_index
            smv_code_list.append(tmp)
            smv_code_list.append(smv_code[number])
            end_logic_number = number + 1
    tmp = ""
    for insert_number,insert_index in enumerate(smv_code):
        if insert_number >= end_logic_number:
                if insert_index != " ":
                    tmp = tmp + insert_index
    smv_code_list.append(tmp)
    return smv_code_list

#dotファイルを書き出すための関数
#書き出されたファイルが意図されたのか見るため。読み込んで出力
def file_w_dot(pydot_list,file_name,dot_graph_dir):
    with open(dot_graph_dir + file_name,"w") as f_w:
        f_w.write("\n".join(pydot_list))

#引き値：充足時の変数の現状態、着目変数、着目変数に関連の変数のリスト
#返り値：dotファイルに書き出すためのリスト
def dot_list_make(check_unique_list,search_val_list,label_list):
    pydot_list = ["digraph G {"]
    for index in check_unique_list:
        split_list = index.split(",")
        #print(split_list)
        for search_number,search_index in enumerate(search_val_list):
            if search_number == 0:
                tmp = search_index + "=" + split_list[0]
                tmp_next =  search_index + "=" + split_list[1]
            else:
                tmp = tmp + "," + search_index + "=" \
                    + split_list[search_number * 2]
                tmp_next = tmp_next + "," + search_index + "=" \
                           + split_list[search_number * 2 + 1]

        pydot_tmp =  "\"" + tmp + "\" -> \"" + tmp_next +"\""
        #pydot_tmp = "\"" + tmp + "\" -> \"" + tmp_next +"\""  + ";"
        search_len = len(search_val_list)
        if len(label_list) >=  1:
            pydot_tmp = pydot_tmp + "[label=" + "\""
            number = 0
            for label_index in label_list:
                if number == 0:
                    #print(number,label_index)
                    pydot_tmp = pydot_tmp + label_index + "=" \
                                + split_list[number + search_len*2]
                else:
                    pydot_tmp = pydot_tmp + " ," + label_index + "=" \
                                + split_list[number +search_len*2]
                number = number + 1
            pydot_tmp = pydot_tmp + "\"];"
        pydot_list.append(pydot_tmp)
    pydot_list.append("}")
    #for index in pydot_list:
        #print(index)
    return pydot_list

#dotファイルを読み込み、グラフを画像ファイルに書き出し、出力する関数
#引き値：ファイル名
def graph_write_show_png(file_name,dot_graph_dir):
    graph=pydot.graphviz.graph_from_dot_file(dot_graph_dir+file_name)
    path = dot_graph_dir + file_name + ".png"
    graph.write_png(path)
    # 画像ファイルパスから読み込み
    img = Image.open(path)
    #画像ファイルを表示
    img.show()

def Z3_inspection_file_write(val_list,search_val_list,
                    label_list,py_file_list,current_dir):
    tmp_now_list = []
    tmp_next_list = []
    add_tmp_list = []
    for row_number, row in enumerate(val_list):
        tmp_for_val = row[0] + "_val"
        tmp_for_next = row[0] + "_val_next"
        tmp_now ="for "
        tmp_next = "for "
        tmp_now = tmp_now + tmp_for_val + " in "
        tmp_next = tmp_next + tmp_for_next + " in "
        add_tmp_list.append( "list[" +  str(row_number) \
                    + "] == "+ tmp_for_val + ",next_list["
                    + str(row_number) +"] == "+ tmp_for_next)

        if row[1] == "range":
            int_tmp = int(row[3]) + 1
            tmp_now = tmp_now + "range(" + row[2]+"," + str(int_tmp)
            tmp_next = tmp_next + "range(" + row[2]+"," + str(int_tmp)
        elif row[1] == "boolean":
            tmp_now = tmp_now + "(True, False"
            tmp_next = tmp_next + "(True, False"
        elif row[1] == "enum":
            tmp_str = ""
            for index_number,index in enumerate(row):
                if index_number > 1:
                    if index_number == len(row) - 1:
                        tmp_str = tmp_str + index
                    else:
                        tmp_str = tmp_str + index + ", "
            tmp_now = tmp_now + "(" + tmp_str
            tmp_next = tmp_next + "(" + tmp_str
        elif row[1] == "enum_int":
            tmp_str = ""
            for index_number,index in enumerate(row):
                if index_number > 1:
                    if index_number == len(row) - 1:
                        tmp_str = tmp_str + index
                    else:
                        tmp_str = tmp_str + index + ", "
            tmp_now = tmp_now + "(" + tmp_str
            tmp_next = tmp_next + "(" + tmp_str
        else:
            print("error")
        tmp_now = tmp_now + "):"
        tmp_next = tmp_next + "):"
        tmp_now_list.append(tmp_now)
        tmp_next_list.append(tmp_next)

    tab_tmp = "    "
    for index in tmp_now_list:
        tmp = tab_tmp + index
        py_file_list.append(tmp)
        tab_tmp = tab_tmp + "    "
    for index in tmp_next_list:
        tmp = tab_tmp + index
        py_file_list.append(tmp)
        tab_tmp = tab_tmp + "    "
    py_file_list.append(tab_tmp + "s.push()")

    add_tmp = "s.add(And("
    number = 0
    for index in add_tmp_list:
        if number > 0:
            add_tmp = add_tmp + ","
        add_tmp = add_tmp + index
        number = number + 1
    add_tmp = add_tmp + "))"

    py_file_list.append(tab_tmp + add_tmp)
    py_file_list.append(tab_tmp + "s_check = s.check()")

    py_file_list.append(tab_tmp + "if s_check == sat:")
    #着目する変数における状態移動がある条件文を作成
    for index in search_val_list:
        if_tmp = "if str(" + index + "_val) != str(" \
                + index + "_val_next):"
        py_file_list.append(tab_tmp + "     " + if_tmp)
        #着目する変数における値をcheck_listに加える文を作成
        str_tmp = ""
        for search_number,search_index in enumerate(search_val_list):
            #print("search_number:"+str(search_number))
            if search_number == 0:
                str_tmp = "          check_list.append(str(" \
                    + search_index + "_val)+\",\" +str(" \
                    + search_index +"_val_next)"
            else:
                str_tmp = str_tmp + "+\",\"+str(" + search_index \
                    + "_val)+\",\" +str("+ search_index +"_val_next)"
        for index in label_list:
            str_tmp = str_tmp + "+\",\"+str(" + index +"_val)"
        str_tmp = str_tmp + ")"
        py_file_list.append(tab_tmp + str_tmp)

    py_file_list.append(tab_tmp + "s.pop()")
    #py_file_list.append("   " + "print(list)")
    #py_file_list.append("   " + "print(next_list)")
    py_file_list.append("    "+ "return check_list")

    with open( current_dir + "Z3_inspection.py" ,"w") as f_w:
        f_w.write("\n".join(py_file_list))

#入力してくれたファイルから、ファイル名のみを抽出して、
#新しいファイル名を作成
def file_name_make(ftmp,search_val_list):
    file_name = ""
    check = False
    end_number = ftmp.rfind("\\")
    if end_number != -1:
        for number,index in enumerate(ftmp):
            if number > end_number:
                if index == ".":
                    check = True
                if check == False:
                    file_name = file_name + index
    if check != False:
        end_number = ftmp.rfind("//")
        if  end_number != -1:
            for number,index in enumerate(ftmp):
                if number > end_number:
                    if index == ".":
                        check = True
                    if check == False:
                        file_name = file_name + index
    for search_number,search_index in enumerate(search_val_list):
            file_name = file_name + "_" +  search_index

    #print(file_name)
    return file_name

#テスト用にコマンドプロンプトに出力
def cmd_output(mylist,init_list,val_list,case_list,constant_list,
    next_list,label_list,search_val_list,val_z3_list):
    #コマンドプロンプトを出力

    print("---mylist---")
    for row in mylist:
        print(row)

    print("---init_list---")
    for row in init_list:
        print(row)

    print("---val_list---")
    for row in val_list:
        print(row)

    print("---case_list---")
    for row in case_list:
        print(row)

    print("---constant_list---")
    for row in constant_list:
        print(row)

    print("---next_list---")
    for row in next_list:
        print(row)

    print("---label_list---")
    print(label_list)

    print("---search_val_list---")
    for index in search_val_list:
        print(index)

    print("---val_z3_list---")
    for row in val_z3_list:
        print(row)


#列挙型である変数をまとめたリストを作成する関数
#引き値:変数をまとめたリスト：val_list
#返り値：列挙型である変数をまとめたリスト
def enum_list_make(val_list):
    enum_list = []
    for row in val_list:
        if row[1] == "enum":
            enum_list.append(row[0])
    return enum_list
#!pのsmvコードをp!のように移動するための関数
#返り値：not_move_smv_code_list
def not_operand_move(Log_exp_list):
    nomove_Log_exp_list = Log_exp_list
    #!の後が(であるかチェックするための変数
    operand_check=False
    not_check = False
    not_number = 0
    for number,index in enumerate(Log_exp_list):
        if not_check == True:
            if Log_exp_list[number] != "(":
                tmp =  nomove_Log_exp_list[number]
                nomove_Log_exp_list[number-1] =tmp
                nomove_Log_exp_list[number] = "!"
            else:
                operand_check = True
                nomove_Log_exp_list.pop(number-1)
            not_check = False

        if index == "!":
            not_check = True

        if index == ")":
            if operand_check == True:
                nomove_Log_exp_list.insert(number+1,"!")
    return nomove_Log_exp_list

#変数名をコマンドプロンプトに出力
def val_print(val_list):
    tmp_list = []
    for row in val_list:
        tmp_list.append(row[0])
    print(tmp_list)

#val_listの複製リストval_z3_listを作成
#ex.val_list = ["fjao,"enum","fa","aaa"]
#ex.val_z3_list = ["fjao,"enum","fa_1","aaa_1"]
def val_z3_list_make(val_list):
    val_z3_list = []
    for row_number,row in enumerate(val_list):
        tmp_list = []
        if row[1] == "enum":
            for index_number,index in enumerate(row):
                if index_number > 1:
                    tmp_list.append(index + "_" + str(row_number))
                else:
                    tmp_list.append(index)
            val_z3_list.append(tmp_list)
        else:
            val_z3_list.append(row)
    return val_z3_list

#論理式におけるz3変更
def Log_val_name_change(val_list,val_z3_list,Log_exp_str):
    Log_val_name_str = ""
    check = False
    for row_number,row in enumerate(val_list):
        if Log_exp_str.find(row[0]) != -1:
            if row[1] == "enum":
                for index_number,index in enumerate(row):
                    if index_number > 1:
                        if Log_exp_str.find(index) != -1:
                            #print(Log_exp_str,index)
                            tmp_number = Log_exp_str.find(index)
                            nagasa =  len(index)
                            number = tmp_number + nagasa
                            Log_val_name_str=Log_exp_str[:number] \
                                            + "_" + str(row_number)
                            check = True
    if check == False:
        Log_val_name_str = Log_exp_str

    return Log_val_name_str

#右辺式における値の変更
def uhensiki_val_change(case_list,val_list):
    for row_number,row in enumerate(case_list):
        for val_row_number,val_row in enumerate(val_list):
            #print(row[0],val_row[0])
            if row[0] == (val_row[0]):
                if val_row[1] == "enum":
                    #print("enum_check")
                    check_number = val_row_number
                    check = True
                    continue
                else:
                    check = False
        if check:
            for index_number,index in enumerate(row):
                #print(index)
                if index_number > 1:
                  for number,v_index in enumerate(val_list[check_number]):
                   if number > 1:
                    if index == v_index:
                      #print("右辺式変更")
                      #print(index,val_index)
                      tmp = case_list[row_number][index_number] \
                                + "_" + str(ch_number)
                      case_list[row_number][index_number] = tmp
    return case_list

#check_unique_list内におけるz3表現をsmv表現に直す
def che_uni_list_val_change(check_unique_list,val_z3_list):
    check_unique_val_change_list = []
    for row_number, row in enumerate(check_unique_list):
        tmp_list = row.split(",")
        #print(tmp_list)
        for index_number,index  in enumerate(tmp_list):
            for val_row_number,val_row in enumerate(val_z3_list):
                for val_index_number,val_index in enumerate(val_row):
                    if val_index_number >1:
                        if index == val_index:
                            if val_row[1] == "enum":
                                buf_list = val_index.split("_")
                                #print(buf_list)
                                tmp_list[index_number] = buf_list[0]
        tmp_str = ""
        for index_number,index in enumerate(tmp_list):
            if index_number > 0:
                tmp_str = tmp_str + ","
            tmp_str = tmp_str + index
        check_unique_val_change_list.append(tmp_str)
    return check_unique_val_change_list


#ソルバで全遷移を検査するファイルを作成する
def make_Z3_inspection_file(val_z3_list,case_list,
    search_val_list,label_list,current_dir):

    py_file_list = ["from Z3_solver import *"]
    py_file_list.append("def Z3_inspect():")
    py_file_list.append("    check_list = []")
    py_file_list.append("    s,list,next_list = solver_make()")
    py_file_list = solver_val_def_file_write(val_z3_list,
                                    case_list,py_file_list)
    Z3_inspection_file_write(val_z3_list,search_val_list,
                            label_list,py_file_list,current_dir)

#書き出したファイル(検査用)を読み込んで、実行する
def execute_Z3_inspection_file(val_z3_list):
    import  Z3_inspection
    check_list = Z3_inspection.Z3_inspect()
    #print("---check_list---")
    #for row in check_list:
    #    print(row)
    check_unique_list = set(check_list)
    #print("---check_unique_list---")
    #for row in check_unique_list:
    #    print(row)

    check_unique_list=che_uni_list_val_change(check_unique_list,
                                                    val_z3_list)

    return check_unique_list

#図に出力するため、dotファイルを作成する
#そのため、リストに書き出して、ファイルに書き込む
def make_dot_file(check_unique_list,search_val_list,
                    label_list,ftmp,dot_graph_dir):
    pydot_list = dot_list_make(check_unique_list,
                        search_val_list,label_list)
    file_name = file_name_make(ftmp,search_val_list)
    file_w_dot(pydot_list,file_name,dot_graph_dir)
    return file_name

#ソルバに制約を加えるため,ファイルに書き出せるリストを作成する
def make_Z3_solver_file(val_z3_list,case_list,current_dir):
    solver_py_file_list = ["from z3 import *"]
    solver_py_file_list.append("def solver_make():")
    solver_py_file_list.append("    s=Solver()")
    solver_py_file_list = solver_val_def_file_write(val_z3_list,
                                case_list,solver_py_file_list)
    solver_py_file_list = solver_change_def_file_write(val_list,
                    val_z3_list,case_list,solver_py_file_list)
    #ファイルに書き出す
    with open(current_dir + "Z3_solver.py" ,"w") as f_w:
        f_w.write("\n".join(solver_py_file_list))

def execute_Z3_solver_file():
    #書き出したファイル(制約用)を読み込んで、実行する
    import Z3_solver
    s,list,next_list = Z3_solver.solver_make()
    return s,list,next_list

def input_smv_search_val():
    #smvファイルを入力
    mylist,ftmp = filetolist()
    mylist = list_null_del(mylist)
    val_list = val_storage(mylist)
    val_list = val_list_extantion(val_list)
    #着目したい変数を入力
    search_val_list =search_val_list_func(val_list)

    return mylist,ftmp,val_list,search_val_list
#smvファイルを字句解析する
def Lexical_analysis_smv_file(mylist,val_list,search_val_list):
    init_list = init_extraction(mylist,val_list)
    case_list = case_extraction_ext(mylist,val_list)
    case_list = uhensiki_val_change(case_list,val_list)
    constant_list = constant_extraction(mylist,val_list)
    next_list = next_list_make(case_list,constant_list)
    enum_list = enum_list_make(val_list)
    val_z3_list = val_z3_list_make(val_list)
    #着目する変数に関連する変数をまとめる
    label_list = label_list_make(next_list,val_list ,search_val_list)

    return (init_list,case_list,constant_list,next_list,
                        enum_list,val_z3_list,label_list)


def path_management():
    #カレントディレクトリのパス設定
    current_dir = "D:/study/study_code/"
    #dotファイルの保存先のフォルダパスを設定
    dot_graph_dir = "D:/study/study_code/pictures/"
    os.chdir(current_dir)
    return current_dir,dot_graph_dir
#実行
if __name__ == '__main__':

    #パスの管理を行う
    current_dir,dot_graph_dir = path_management()

    #smvファイル、着目したい変数を入力する
    mylist,ftmp,val_list,search_val_list = input_smv_search_val()

    #smvファイルを字句解析する
    (init_list,case_list,constant_list,next_list,enum_list,val_z3_list,
                        label_list)=Lexical_analysis_smv_file(mylist,
                                            val_list,search_val_list)

    #コマンドプロンプトに出力して、確認
    cmd_output(mylist,init_list,val_list,case_list,constant_list,
                next_list,label_list,search_val_list,val_z3_list)

    #ソルバに制約を加えるため,ファイルに書き出せるリストを作成する
    make_Z3_solver_file(val_z3_list,case_list,current_dir)

    #書き出したファイル(制約用)を読み込んで、実行する
    execute_Z3_solver_file()

    #ソルバで全遷移を検査するファイルを作成する
    make_Z3_inspection_file(val_z3_list,case_list,search_val_list,
                                            label_list,current_dir)

    #書き出したファイル(検査用)を読み込んで、実行する
    check_unique_list = execute_Z3_inspection_file(val_z3_list)
    for row in check_unique_list:
        print(row)
    #図に出力するため、dotファイルを作成する。
    #そのためにリストに書き出して、ファイルに書き込む
    file_name = make_dot_file(check_unique_list,search_val_list,
                                label_list,ftmp,dot_graph_dir)

    #図を保存し、出力
    graph_write_show_png(file_name,dot_graph_dir)
